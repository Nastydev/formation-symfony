<?php

namespace App\DataFixtures;

use App\Entity\Genre;
use App\Entity\Movie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $genres = [
            'Action',
            'Romantique',
            'Comique'
        ];

        $movies = [
            'Fight Club' => 'test test test',
            'Snatch' => 'bla bla bla',
            'Pulp Fiction' => 'ok ok ok ok'
        ];

        foreach ($genres as $genre) {
            $newGenre = new Genre();
            $newGenre->setName($genre);
            $manager->persist($newGenre);
        }

        foreach ($movies as $key => $value) {
            $newMovie = new Movie();
            $poster = $key . '.png';
            $newMovie->setTitle($key);
            $newMovie->setPlot($value);
            $newMovie->setPoster($poster);
            $newMovie->setReleasedAt(new \DateTimeImmutable());
            $newMovie->addGenre($newGenre);
            $manager->persist($newMovie);
        }

        $manager->flush();
    }
}
