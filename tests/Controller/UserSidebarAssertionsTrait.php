<?php

namespace App\Tests\Controller;

trait UserSidebarAssertionsTrait
{
    public static function assertUserSidebarLinkIsActive(string $linkText, string $message = ''): void
    {
        self::assertSelectorTextSame('main nav#sidebar li.nav-item.active a.nav-link', $linkText, $message);
    }
}
