<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MovieControllerTest extends WebTestCase
{
    use HeaderNavAssertionsTrait;

    public function testTopRated(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/movie/top-rated');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderLinkIsActive('Top Rated');
    }

    public function testByGenre(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/movie/by-genre');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderLinkIsActive('Browse By Genre');
    }

    public function testLatest(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/movie/latest');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderLinkIsActive('Latest');
    }
}
