<?php

namespace App\Controller;

use App\Services\OmdbApiClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @var OmdbApiClient
     */
    private $omdbApiClient;

    public function __construct(
        OmdbApiClient $omdbApiClient
    )
    {
        $this->omdbApiClient = $omdbApiClient;
    }

    /**
     * @Route("/", name="app_main_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('main/index.html.twig');
    }

    /**
     * @Route("/contact", name="app_main_contact", methods={"GET"})
     */
    public function contact(): Response
    {
        return $this->render('main/contact.html.twig');
    }

    /**
     * @Route("/login", name="app_main_login", methods={"GET"})
     */
    public function login(): Response
    {
        return $this->render('main/login.html.twig');
    }

    /**
     * @Route("/register", name="app_main_register", methods={"GET"})
     */
    public function register(): Response
    {
        return $this->render('main/register.html.twig');
    }

    /**
     * @Route("/password-recovering", name="app_main_password_recovering", methods={"GET"})
     */
    public function passwordRecovering(): Response
    {
        return $this->render('main/password_recovering.html.twig');
    }

    /**
     * @Route("/trailer-player", name="app_main_trailer_player", methods={"GET"})
     */
    public function trailerPlayer(): Response
    {
        return $this->render('main/trailer_player.html.twig');
    }

    /**
     * @Route("/fetch", name="app_main_fetch")
     */
    public function fetch()
    {
        $movies = $this->omdbApiClient->getByTitle('snatch');

        dd($movies);
    }
}
