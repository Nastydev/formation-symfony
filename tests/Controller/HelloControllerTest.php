<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class HelloControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/hello');

        self::assertSame('Hello World!', $client->getResponse()->getContent());
    }

    public function testIndexWithName(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/hello/toto');

        self::assertSame('Hello toto!', $client->getResponse()->getContent());
    }
}
