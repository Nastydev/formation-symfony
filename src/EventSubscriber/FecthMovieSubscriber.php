<?php

namespace App\EventSubscriber;

use App\Controller\AdminMovieController;
use App\Entity\Movie;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FecthMovieSubscriber implements EventSubscriberInterface
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function movie($event)
    {
        $this->logger->info('xxx nouveau film ajouté xxx');
    }

    public static function getSubscribedEvents()
    {
        return [
           'fetch_movie' => 'movie',
        ];
    }
}
