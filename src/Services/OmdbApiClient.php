<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class OmdbApiClient
{
    private HttpClientInterface $client;
    private string $appOmbApiKey;
    private string $appOmbApiHost;

    public function __construct(
        HttpClientInterface $client,
        string $appOmbApiKey,
        string $appOmbApiHost
    )
    {
        $this->client = $client;
        $this->appOmbApiKey = $appOmbApiKey;
        $this->appOmbApiHost = $appOmbApiHost;
    }


    public function getByTitle(string $title): array
    {
        return $this->client->request('GET', $this->appOmbApiHost, ['query' => [
            'apikey' => $this->appOmbApiKey,
            't' => $title,
        ]])->toArray();
    }
}