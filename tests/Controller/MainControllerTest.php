<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainControllerTest extends WebTestCase
{
    use HeaderNavAssertionsTrait;

    public function testIndex(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderLinkIsActive('Home');
    }

    public function testContact(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/contact');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderLinkIsActive('Help Center');
    }

    public function testLogin(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/login');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderFaLinkIsActive('user');
    }
}
