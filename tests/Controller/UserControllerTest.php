<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends WebTestCase
{
    use HeaderNavAssertionsTrait;
    use UserSidebarAssertionsTrait;

    public function testCart(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/user/cart');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertHeaderFaLinkIsActive('shopping-bag');
    }

    public function testProfile(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/user/profile');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertUserSidebarLinkIsActive('Profile');
    }

    public function testWatchlist(): void
    {
        $client = self::createClient();

        $client->request(Request::METHOD_GET, '/user/watchlist');

        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertUserSidebarLinkIsActive('Watchlist');
    }
}
