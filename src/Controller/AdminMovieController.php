<?php

namespace App\Controller;

use App\Entity\Movie;
use App\EventSubscriber\FecthMovieSubscriber;
use App\Form\MovieType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @Route("/admin/movie", name="app_admin_")
 */
class AdminMovieController extends AbstractController
{
    /**
     * @Route("/new", name="move_new")
     */
    public function new(Request $request, EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher): Response
    {
        $newMovie = new Movie();
        $form = $this->createForm(MovieType::class, $newMovie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($newMovie);
            $em->flush();

            $eventDispatcher->dispatch(new Event(), 'movie');

            return $this->redirectToRoute('app_main_index');
        }

        return $this->render('admin_movie/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
