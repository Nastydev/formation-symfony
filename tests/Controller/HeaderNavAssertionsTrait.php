<?php

namespace App\Tests\Controller;

trait HeaderNavAssertionsTrait
{
    public static function assertHeaderLinkIsActive(string $linkText, string $message = ''): void
    {
        self::assertSelectorTextSame('header nav li.nav-item.active a.nav-link', "{$linkText} (current)", $message);
    }

    public static function assertHeaderFaLinkIsActive(string $fa, string $message = ''): void
    {
        // use "svg.fa-" instead of "i.fa-" if JS is executed (e.g using Panther)
        self::assertSelectorExists('header nav li.nav-item.active a.nav-link i.fa-'.$fa, $message);
    }
}
