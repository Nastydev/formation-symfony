<?php

namespace App\Command;

use App\Entity\Movie;
use App\Form\MovieType;
use App\Repository\MovieRepository;
use App\Services\OmdbApiClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class MovieNewCommand extends Command
{
    private $apiClient;
    private $entityManager;

    public function __construct(OmdbApiClient $apiClient, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->apiClient = $apiClient;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:movie:new')
            ->setDescription('Add a short description for your command')
            ->addArgument('title', InputArgument::REQUIRED, 'Title of the movie')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $title = $input->getArgument('title');

        $max = 5; $ratio = 1;
        $progressBar = new ProgressBar($output, $max);
        $progressBar->setFormat(" \033[44;37m %title:-45s% \033[0m\n %current%/%max% %bar% %percent:3s%%\n 🏁 Estimated time remaining : %remaining:-8s% %memory:0s%\n Time : <fg=green>%elapsed%</>");
        $progressBar->setBarCharacter("\033[32m●\033[0m");
        $progressBar->setEmptyBarCharacter("\033[31m●\033[0m");
        $progressBar->setMessage('Movie', 'title');
        // If you're iterating over a large number of items, consider setting the redraw frequency
        $progressBar->setRedrawFrequency($max / $ratio);

        $progressBar->start();

        $apiMovie = $this->apiClient->getByTitle($title);
        $progressBar->advance();sleep(1);
        $date = \DateTime::createFromFormat('d M Y', $apiMovie['Released']);
        $progressBar->advance();sleep(1);
        $movie = new Movie();
        $movie
            ->setTitle($apiMovie['Title'])
            ->setPoster($apiMovie['Poster'])
            ->setPlot($apiMovie['Plot'])
            ->setReleasedAt($date)
        ;
        $progressBar->advance();sleep(1);

        $this->entityManager->persist($movie);
        $this->entityManager->flush();
        $progressBar->advance();sleep(1);


        $allMovie = $this->entityManager->getRepository(Movie::class)->findAll();
        $count = count($allMovie);

        $progressBar->advance();sleep(1);

        $progressBar->finish();

        $io->success(sprintf('The movie %s have been imported', $title));
        $io->title(sprintf('There is %s movies in the database', $count));

        return Command::SUCCESS;
    }
}