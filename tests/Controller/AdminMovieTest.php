<?php

namespace App\Tests\Controller;

use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AdminMovieTest extends WebTestCase
{
    use HeaderNavAssertionsTrait;

    /**
     * @group form
     */
    public function testForm(): void
    {
        $client = self::createClient();

        $crawler = $client->request(Request::METHOD_GET, '/admin/movie/new');
        $content = $client->getResponse()->getContent();
        self::assertStringContainsString('<form', $content);
        self::assertStringContainsString('Save', $content);

        $form = $crawler->selectButton('Save')->form();
        $form['movie[title]'] = 'Fabien';
        $form['movie[plot]'] = 'Symfony rocks!';
        $form['movie[poster]'] = 'images.png';

        $form['movie[releasedAt][date][year]'] = '2020';
        $form['movie[releasedAt][date][month]'] = '4';
        $form['movie[releasedAt][date][day]'] = '4';
        $form['movie[releasedAt][time][hour]'] = '1';
        $form['movie[releasedAt][time][minute]'] = '1';

        $client->submit($form);

        $response = $client->getResponse();
        self::assertTrue($response->isRedirect('/'));
    }
}