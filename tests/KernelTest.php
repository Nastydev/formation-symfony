<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class KernelTest extends WebTestCase
{
    private $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = self::createClient();
    }

    protected function tearDown(): void
    {
        $this->client = null;

        parent::tearDown();
    }

    /**
     * @dataProvider provideRoutes
     */
    public function testRouting(string $method, string $url, int $statusCode, string $route, string $pageTitle = null): void
    {
        $this->client->request($method, $url);

        self::assertRouteSame($route);
        self::assertResponseStatusCodeSame($statusCode);

        if ($pageTitle) {
            self::assertPageTitleSame($pageTitle, 'Title should match.');
        }
    }

    public function provideRoutes(): iterable
    {
        // Main
        yield 'Main index' => [Request::METHOD_GET, '/', Response::HTTP_OK, 'app_main_index', 'SensioTV | Home'];
        yield 'Main contact' => [Request::METHOD_GET, '/contact', Response::HTTP_OK, 'app_main_contact', 'SensioTV | Contact'];
        yield 'Main login' => [Request::METHOD_GET, '/login', Response::HTTP_OK, 'app_main_login', 'SensioTV | Log In'];
        yield 'Main password recovering' => [Request::METHOD_GET, '/password-recovering', Response::HTTP_OK, 'app_main_password_recovering', 'SensioTV | Password Recovery'];
        yield 'Main register' => [Request::METHOD_GET, '/register', Response::HTTP_OK, 'app_main_register', 'SensioTV | Registration'];
        yield 'Main trailer player' => [Request::METHOD_GET, '/trailer-player', Response::HTTP_OK, 'app_main_trailer_player', 'SensioTV | Trailer Player'];

        // Movie
        yield 'Movie by genre' => [Request::METHOD_GET, '/movie/by-genre', Response::HTTP_OK, 'app_movie_by_genre', 'SensioTV | Movies By Genre'];
        yield 'Movie details' => [Request::METHOD_GET, '/movie/details', Response::HTTP_OK, 'app_movie_details', 'SensioTV | Avengers: Endgame Details'];
        yield 'Movie latest' => [Request::METHOD_GET, '/movie/latest', Response::HTTP_OK, 'app_movie_latest', 'SensioTV | Latest Movies'];
        yield 'Movie player' => [Request::METHOD_GET, '/movie/player', Response::HTTP_OK, 'app_movie_player', 'SensioTV | Movie Player'];
        yield 'Movie top rated' => [Request::METHOD_GET, '/movie/top-rated', Response::HTTP_OK, 'app_movie_top_rated', 'SensioTV | Top Rated Movies'];

        // User
        yield 'User cart' => [Request::METHOD_GET, '/user/cart', Response::HTTP_OK, 'app_user_cart', 'SensioTV | Shopping cart'];
        yield 'User profile' => [Request::METHOD_GET, '/user/profile', Response::HTTP_OK, 'app_user_profile', 'SensioTV | My Account'];
        yield 'User watchlist' => [Request::METHOD_GET, '/user/watchlist', Response::HTTP_OK, 'app_user_watchlist', 'SensioTV | My Watchlist'];
    }
}
