<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/cart", name="app_user_cart", methods={"GET"})
     */
    public function cart(): Response
    {
        return $this->render('user/cart.html.twig');
    }

    /**
     * @Route("/profile", name="app_user_profile", methods={"GET"})
     */
    public function profile(): Response
    {
        return $this->render('user/profile.html.twig');
    }

    /**
     * @Route("/watchlist", name="app_user_watchlist", methods={"GET"})
     */
    public function watchlist(): Response
    {
        return $this->render('user/watchlist.html.twig');
    }
}
